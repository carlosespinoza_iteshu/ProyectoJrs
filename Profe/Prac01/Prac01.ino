void setup() {
  // put your setup code here, to run once:
  pinMode(13,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13,HIGH); //HIGH =5v
  delay(150); //funciona con milisegundos  :-  1000=1seg
  //mantengo encendido
  digitalWrite(13,LOW); //LOW=0v
  delay(150); 
  //mantengo apagado
}
